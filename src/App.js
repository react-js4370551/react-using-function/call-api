import logo from './logo.svg';
import './App.css';
import UsingFetch from './component/using-fetch';
import UsingUseeffectFetch from './component/using-useeffect-fetch';
import UsingAxios from './component/using-axios';
import { AjaxCall } from './component/ajax-call';
import UsingAsyncFetch from './component/using-async-fetch';

function App() {
  return (
    <div className="App">
      {/* <UsingFetch /> */}
      {/* <UsingUseeffectFetch /> */}
      {/* <AjaxCall /> */}
      {/* <UsingAxios /> */}
      <UsingAsyncFetch />
    </div>
  );
}

export default App;
