import React, { useState } from 'react'

export default function UsingAsyncFetch() {
    const [loading, setLoading] = useState(false);
    //here in useState if we not initialise {photos:[]}, then we will get error saying photoes not define. 
    const [marse, setMarse] = useState({ photos: [] });
    async function callApiOnClick() {
        const response = await fetch('https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=INcFXcvcQWafWQ3OhEEptQNLuCZYy1MaSUYFs5pZ');
        const data = await response.json();
        setMarse(data);
        setLoading(true);
    }

    return (
        <div>
            <button onClick={callApiOnClick}>fetchAPP</button>
            <hr />
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>CameraName</th>
                        <th>Image</th>
                        <th>RovarName</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        //loading indicate api got caal and reposne is avaliable in marse.
                        //also we will get error, becouse this html tag got called once component got loaded after server start once(life cycle method).
                        //due to this marse.photos, not able to see photos inside marse.
                        //if you dont want to use loading then to call api, we to call inside useEffect().
                        loading === true ? marse.photos.map((item) =>
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td>{item.camera.full_name}</td>
                                <td><img src={item.img_src} width="150" height="150" /></td>
                                <td>{item.rover.name}</td>
                                <td>{item.rover.status}</td>
                            </tr>)
                            : <h4>Loading</h4>
                    }
                </tbody>
            </table>
        </div>
    )
}
