import React, { useEffect, useState } from 'react'
//api source ::
//https://api.nasa.gov/
export default function UsingUseeffectFetch() {
    //here in useState if we not initialise {photos:[]}, then we will get error saying photoes not define. 
    const [marse, setMarse] = useState({ photos: [] });
    const [errors, setErrors] = useState({});
    useEffect(() => {
        fetch('https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=INcFXcvcQWafWQ3OhEEptQNLuCZYy1MaSUYFs5pZ')
            //response coming in binary formate to convert it into json
            .then((binaryResponse) => binaryResponse.json())
            //set response setMarse state.
            .then((response) =>
                setMarse(response),
            ).catch((error) =>
                setErrors(error)
            )
    }, [])
    //here passing empty array means, we want to execute useEffect method only once.

    return (
        <div>
            {/* <p>{JSON.stringify(marse)}</p> */}
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>CameraName</th>
                        <th>Image</th>
                        <th>RovarName</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        //loading indicate api got caal and reposne is avaliable in marse.
                        //also we will get error, becouse this html tag got called once component got loaded after server start once(life cycle method).
                        //due to this marse.photos, not able to see photos inside marse.
                        //if you dont want to use loading then to call api, we to call inside useEffect().
                        marse.photos.map((item) =>
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td>{item.camera.full_name}</td>
                                <td><img src={item.img_src} width="150" height='150'/></td>
                                <td>{item.rover.name}</td>
                                <td>{item.rover.status}</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </div>
    )
}
