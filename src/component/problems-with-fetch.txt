Q: WHat is the issue with axios?
Ans : 
  => It return data in binary format.
  => You Have to parse data into json
  => Browser may block COM marshalling[Common Object Model]
  => CORS issue [Cross Origin Resourse sharing]
  => Fecth using cathc block to handle error which is poor to handle the error.


  To SOlve above Problem we can use :
  1. jquery ajax call :
     => it return data into json format, so no need to convert inot json
     => Handle CORS issue
     => It reduces the browser compability issue.
     => it provide lot of ajax cycle method which can use to track issue in api call:

        1) ajaxStart()
        2) ajaxSTop()
        3) ajaxSuccess()
        4) ajaxError()
        5) ajaxCOmplete()

        Syntax :

          $.ajax({
            method:"GET || POST ||PUT|| PATCH",
            url : "url",
            success : (reposne) => {
                steCatogries(reposne);
            },
            error : (error) => {
                console.log(error);
            }
          })

          To use jquery in react first we need to install:
          => npm install jquery --save

  2. axios api call:

    => it return json data
    => it is good in error handling, more featur then jquery ajax.
    => is it more secured
       1) prevent CORS attack
       2) prevent XSS attack
       3) cross page posting
    => it can handle multiple requst simultaniously.
    => it is async without blocking current request, it can process another request.
    => it is faster.

    install axios :

    npm install axios --save





  
 